package uk.co.massimocarli.starlingtest.business

import org.hamcrest.core.IsEqual.equalTo
import org.junit.Assert
import org.junit.Assert.assertThat
import org.junit.Test

class RoundUpUtilTest {

    //@Test(expected = IllegalStateException::class)
    @Test
    fun roundUp_positiveNumber_throwsException() {
        val roundUp = roundUp(1.0)
        assertThat(roundUp, equalTo(0.0))
    }

    @Test
    fun roundUp_negativeZero_isZero() {
        val roundUp = roundUp(-0.0)
        assertThat(roundUp, equalTo(0.0))
    }

    @Test
    fun roundUp_zero_isZero() {
        val roundUp = roundUp(0.0)
        assertThat(roundUp, equalTo(0.0))
    }

    @Test
    fun roundUp_noDecimal_isZero() {
        val roundUp = roundUp(-3.0)
        assertThat(roundUp, equalTo(0.0))
    }

    @Test
    fun roundUp_singleDecimal_isTheRoundUp() {
        val roundUp = roundUp(-3.1)
        Assert.assertEquals(0.9, roundUp, 0.01)
    }

    @Test
    fun roundUp_doubleDecimal_isTheRoundUp() {
        val roundUp = roundUp(-3.14)
        Assert.assertEquals(0.86, roundUp, 0.001)
    }

    @Test
    fun roundUp_normalAmount_isTheRoundUp() {
        val roundUp = roundUp(-156.23)
        Assert.assertEquals(0.77, roundUp, 0.001)
    }

    @Test
    fun roundUp_closeToNextPound_isTheRoundUp() {
        val roundUp = roundUp(-45.99)
        Assert.assertEquals(0.01, roundUp, 0.001)
    }
}