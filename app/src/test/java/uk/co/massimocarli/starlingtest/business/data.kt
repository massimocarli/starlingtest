package uk.co.massimocarli.starlingtest.business

import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.model.Transaction
import java.util.*

val TRANSACTION_1 = Transaction("id",
        "GBP",
        -10.0,
        DIRECTION_OUTBOUND,
        Date(),
        "Test",
        "Test",
        0.0)
val TRANSACTION_2 = Transaction("id",
        "GBP",
        -9.3,
        DIRECTION_OUTBOUND,
        Date(),
        "Test",
        "Test",
        90.0)
val TRANSACTION_3 = Transaction("id",
        "GBP",
        -2.9,
        DIRECTION_OUTBOUND,
        Date(),
        "Test",
        "Test",
        87.1)
val INBOUND_TRANSACTION = Transaction("id",
        "GBP",
        -2.9,
        DIRECTION_INBOUND,
        Date(),
        "Test",
        "Test",
        87.1)

val ZERO_ROUNDUP_TRANSACTION = RoundedUpTransaction(TRANSACTION_1, 0.0)
val ROUNDUP_INBOUND_TRANSACTION_1 = RoundedUpTransaction(INBOUND_TRANSACTION, 0.0)
val ROUNDUP_OUTBOUND_TRANSACTION_2 = RoundedUpTransaction(TRANSACTION_2, 0.7)
val ROUNDUP_OUTBOUND_TRANSACTION_3 = RoundedUpTransaction(TRANSACTION_3, 0.1)