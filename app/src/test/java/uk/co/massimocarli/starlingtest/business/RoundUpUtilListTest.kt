package uk.co.massimocarli.starlingtest.business

import org.hamcrest.core.IsEqual
import org.junit.Assert
import org.junit.Assert.assertTrue
import org.junit.Test
import uk.co.massimocarli.starlingtest.model.Transaction

class RoundUpUtilListTest {

    @Test
    fun roundUpList_emptyList_returnsZero() {
        val input = listOf<Transaction>()
        val roundUp = roundUp(input)
        assertTrue(roundUp.isEmpty())
    }

    @Test
    fun roundUpList_singleItemListWithoutRoundUp_returnsZero() {
        val input = listOf<Transaction>(TRANSACTION_1)
        val roundUp = roundUp(input)
        val enhancedTransaction = roundUp[0]
        Assert.assertThat(enhancedTransaction.roundUp, IsEqual.equalTo(0.0))
    }

    @Test
    fun roundUpList_multipleItemsListWithoutRoundUp_returnsCorrect() {
        val input = listOf<Transaction>(TRANSACTION_1, TRANSACTION_2, TRANSACTION_3)
        val roundUp = roundUp(input)
        Assert.assertEquals(roundUp[0].roundUp, 0.0, 0.0001)
        Assert.assertEquals(roundUp[1].roundUp, 0.7, 0.0001)
        Assert.assertEquals(roundUp[2].roundUp, 0.1, 0.0001)
    }

    @Test
    fun roundUpList_multipleItemsListWithoutRoundUp_skipInbound() {
        val input = listOf<Transaction>(TRANSACTION_1, INBOUND_TRANSACTION, TRANSACTION_3)
        val roundUp = roundUp(input)
        Assert.assertEquals(roundUp[0].roundUp, 0.0, 0.0001)
        Assert.assertEquals(roundUp[1].roundUp, 0.0, 0.0001)
        Assert.assertEquals(roundUp[2].roundUp, 0.1, 0.0001)
    }
}