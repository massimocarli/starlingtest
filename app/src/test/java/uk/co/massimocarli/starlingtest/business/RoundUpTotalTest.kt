package uk.co.massimocarli.starlingtest.business

import org.hamcrest.core.IsEqual
import org.junit.Assert.assertThat
import org.junit.Test
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction

class RoundUpTotalTest {

    @Test
    fun totalRoundUp_emptyList_returnsZero() {
        val transactions = listOf<RoundedUpTransaction>()
        val roundUp = totalRoundUp(transactions)
        assertThat(roundUp, IsEqual.equalTo(0.0))
    }

    @Test
    fun totalRoundUp_singleNoRoundUpItem_returnsZero() {
        val transactions = listOf<RoundedUpTransaction>(ZERO_ROUNDUP_TRANSACTION)
        val roundUp = totalRoundUp(transactions)
        assertThat(roundUp, IsEqual.equalTo(0.0))
    }

    @Test
    fun totalRoundUp_singleWithRoundUp_returnsSame() {
        val transactions = listOf<RoundedUpTransaction>(ROUNDUP_OUTBOUND_TRANSACTION_2)
        val roundUp = totalRoundUp(transactions)
        assertThat(roundUp, IsEqual.equalTo(ROUNDUP_OUTBOUND_TRANSACTION_2.roundUp))
    }

    @Test
    fun totalRoundUp_doubleWithRoundUp_returnsSum() {
        val transactions = listOf<RoundedUpTransaction>(ROUNDUP_OUTBOUND_TRANSACTION_2, ROUNDUP_OUTBOUND_TRANSACTION_3)
        val roundUp = totalRoundUp(transactions)
        assertThat(roundUp, IsEqual.equalTo(ROUNDUP_OUTBOUND_TRANSACTION_2.roundUp + ROUNDUP_OUTBOUND_TRANSACTION_3.roundUp))
    }

    @Test
    fun totalRoundUp_tripleWithRoundUp_ignoreInbound() {
        val transactions = listOf<RoundedUpTransaction>(ROUNDUP_INBOUND_TRANSACTION_1,
                ROUNDUP_OUTBOUND_TRANSACTION_2,
                ROUNDUP_OUTBOUND_TRANSACTION_3)
        val roundUp = totalRoundUp(transactions)
        assertThat(roundUp, IsEqual.equalTo(ROUNDUP_OUTBOUND_TRANSACTION_2.roundUp + ROUNDUP_OUTBOUND_TRANSACTION_3.roundUp))
    }
}