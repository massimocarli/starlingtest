package uk.co.massimocarli.starlingtest.ui.transaction

import io.reactivex.Single
import uk.co.massimocarli.starlingtest.model.*
import uk.co.massimocarli.starlingtest.mvp.UseCase

/**
 * Business logic for the Transactions
 */
interface TransactionUseCase : UseCase {

    /**
     * Returns a Single for the List of RoundedUpTransaction
     */
    fun getTransactionList(): Single<List<RoundedUpTransaction>>

    /**
     * Allow the creation of a saving goal
     */
    fun createSavingGoal(savingGoal: SavingGoal): Single<SavingGoalResponse>

    /**
     * Add money to a saving goal
     */
    fun addMoneyToSavingGoal(savingGoal: SavingGoal, savingAmount: SavingAmount): Single<SavingGoalResponse>

    /**
     * Calculate the total roundUp for transaction
     */
    fun calculateTotalRoundUp(transactions: List<RoundedUpTransaction>): Double
}