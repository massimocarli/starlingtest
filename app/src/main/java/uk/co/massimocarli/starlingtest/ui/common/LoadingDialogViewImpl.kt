package uk.co.massimocarli.starlingtest.ui.common

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import uk.co.massimocarli.starlingtest.R

/**
 * Implementation for the LoadingDialogView
 */
class LoadingDialogViewImpl(val context: Context) : LoadingDialogView {

    var mAlertDialog: AlertDialog? = null

    override fun showLoadingDialog() {
        if (mAlertDialog != null) {
            return
        }
        val builder = AlertDialog.Builder(context)
        val loadingView = LayoutInflater.from(context).inflate(R.layout.loading_dialog_layout, null)
        builder.setView(loadingView)
        builder.setCancelable(false)
        mAlertDialog = builder.show()
    }

    override fun dismissLoadingDialog() {
        mAlertDialog?.dismiss()
        mAlertDialog = null
    }
}