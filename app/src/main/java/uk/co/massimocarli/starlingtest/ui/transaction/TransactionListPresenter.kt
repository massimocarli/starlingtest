package uk.co.massimocarli.starlingtest.ui.transaction

import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.mvp.Presenter

/**
 * The Presenter for the TransactionList
 */
interface TransactionListPresenter : Presenter<TransactionListView> {

    /**
     * Load the Transaction list
     */
    fun loadTransaction()

    /**
     * Allows to save money on SavingGoal
     */
    fun saveMoneyToSavingGoal(name: String, model: List<RoundedUpTransaction>)

    /**
     * Displays the total roundup
     */
    fun displayTotalRoundUp(model: List<RoundedUpTransaction>)
}