package uk.co.massimocarli.starlingtest.ui.common

import androidx.appcompat.app.AppCompatActivity
import uk.co.massimocarli.starlingtest.StarlingApp
import uk.co.massimocarli.starlingtest.dagger.StarlingComponent
import uk.co.massimocarli.starlingtest.mvp.Presenter
import uk.co.massimocarli.starlingtest.mvp.View
import javax.inject.Inject

/**
 * BaseActivity that manages the common behaviour in terms of the relation with presenter
 */
abstract class BaseActivity<P : Presenter<V>, V : View> : AppCompatActivity() {

    var mPresenter: P? = null
        @Inject set
        get

    override fun onStart() {
        mPresenter!!.attach(getView())
        super.onStart()
    }

    override fun onStop() {
        mPresenter!!.detach()
        mPresenter = null
        super.onStop()
    }

    /**
     * Activities have to define who is the View
     */
    protected fun getView(): V {
        return this as V
    }

    protected fun getComp(): StarlingComponent {
        return (application as StarlingApp).getStarlingComponent()
    }
}