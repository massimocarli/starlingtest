package uk.co.massimocarli.starlingtest.model

import java.util.*

/**
 * Model for the response of a transaction list request
 */
data class TransactionResponse(val _embedded: Embedded)

/**
 * Reflects what's returned by the server for the list of Transaction
 */
data class Embedded(val transactions: List<Transaction>)

/**
 * Model for a single transaction
 */
data class Transaction(val id: String,
                       val currency: String = "GPB",
                       val amount: Double,
                       val direction: String,
                       val created: Date,
                       val narrative: String,
                       val source: String,
                       val balance: Double)


/**
 * Data classes cannot be extends in favor of composition. This is the model for the
 * Transaction plus the roundUp
 */
data class RoundedUpTransaction(val transaction: Transaction,
                                val roundUp: Double = 0.0)

/**
 * Model we use for the saving goal creation request
 */
data class SavingGoal(var name: String,
                      var savingsGoalUid: String = "",
                      val currency: String = "GPB",
                      val target: SgTarget)

/**
 * The model we use when we add import to a saving goal
 */
data class SavingAmount(var transferUid: String = "",
                        val amount: SgTarget)

/**
 * Encapsulate the amount for the saving goal
 */
data class SgTarget(val currency: String = "GPB",
                    val minorUnits: Int)

/**
 * Model for the response of a saving goal service
 */
data class SavingGoalResponse(val savingsGoalUid: String,
                              val success: Boolean)