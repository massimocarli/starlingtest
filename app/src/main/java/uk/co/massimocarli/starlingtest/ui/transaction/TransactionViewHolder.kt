package uk.co.massimocarli.starlingtest.ui.transaction

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uk.co.massimocarli.starlingtest.R
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import java.text.SimpleDateFormat

/**
 * ViewHolder implementation for the transaction list
 */
class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        val DATE_FORMATTER = SimpleDateFormat("dd/MM/yy");
    }

    val narrativeTextView: TextView
    val dateTextView: TextView
    val amountView: TextView
    val roundUpTextView: TextView

    init {
        dateTextView = view.findViewById(R.id.transactionDate)
        narrativeTextView = view.findViewById(R.id.narrative)
        amountView = view.findViewById(R.id.amount)
        roundUpTextView = view.findViewById(R.id.roundUp)
    }

    /**
     * Displays the model for the given item
     */
    fun displayModel(model: RoundedUpTransaction) {
        narrativeTextView.text = model.transaction.narrative
        dateTextView.text = DATE_FORMATTER.format(model.transaction.created)
        amountView.text = amountView.context.getString(R.string.amount_output, model.transaction.amount)
        roundUpTextView.text = roundUpTextView.context.getString(R.string.item_round_up, model.roundUp)
        roundUpTextView.visibility = if (model.roundUp > 0.0) View.VISIBLE else View.GONE
    }
}