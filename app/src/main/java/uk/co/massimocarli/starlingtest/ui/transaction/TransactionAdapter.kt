package uk.co.massimocarli.starlingtest.ui.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uk.co.massimocarli.starlingtest.R
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction

/**
 * Adapter implementation for the Transaction list
 */
class TransactionAdapter(val model: List<RoundedUpTransaction>) : RecyclerView.Adapter<TransactionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.transaction_list_item, parent, false)
        return TransactionViewHolder(view)
    }

    override fun getItemCount(): Int = model.size

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.displayModel(model[position])
    }
}