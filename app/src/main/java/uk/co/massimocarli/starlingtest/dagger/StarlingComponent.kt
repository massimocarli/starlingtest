package uk.co.massimocarli.starlingtest.dagger

import dagger.Component
import uk.co.massimocarli.starlingtest.ui.transaction.TransactionListActivity
import javax.inject.Singleton

@Component(modules = arrayOf(NetworkModule::class, AppModule::class, ActivityModule::class))
@Singleton
interface StarlingComponent {

    fun inject(transactionListActivity: TransactionListActivity)
}