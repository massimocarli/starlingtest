package uk.co.massimocarli.starlingtest.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import uk.co.massimocarli.starlingtest.business.common.UidFactory
import uk.co.massimocarli.starlingtest.business.common.UidFactoryImpl
import uk.co.massimocarli.starlingtest.business.networking.StarlingApi
import uk.co.massimocarli.starlingtest.business.repository.TransactionRepository
import uk.co.massimocarli.starlingtest.business.repository.TransactionRepositoryImpl
import uk.co.massimocarli.starlingtest.ui.transaction.TransactionUseCase
import uk.co.massimocarli.starlingtest.ui.transaction.TransactionUseCaseImpl
import javax.inject.Singleton

@Module
class AppModule(val context: Context) {
    @Provides
    @Singleton
    fun provideContext() = context

    @Provides
    @Singleton
    fun provideUidFactory(): UidFactory = UidFactoryImpl()

    @Provides
    @Singleton
    fun provideTransactionUseCase(transactionRepository: TransactionRepository): TransactionUseCase =
            TransactionUseCaseImpl(transactionRepository)

    @Provides
    @Singleton
    fun provideTransactionRepository(starlingApi: StarlingApi): TransactionRepository =
            TransactionRepositoryImpl(starlingApi)
}