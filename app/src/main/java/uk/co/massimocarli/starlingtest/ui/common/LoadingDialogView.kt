package uk.co.massimocarli.starlingtest.ui.common

import uk.co.massimocarli.starlingtest.mvp.View

/**
 * Abstraction for LoadingDialog management
 */
interface LoadingDialogView : View {

    /**
     * Show Loading dialog
     */
    fun showLoadingDialog()

    /**
     * Dispose the loading dialog
     */
    fun dismissLoadingDialog()
}