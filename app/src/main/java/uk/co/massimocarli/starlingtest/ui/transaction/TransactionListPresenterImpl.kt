package uk.co.massimocarli.starlingtest.ui.transaction

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import uk.co.massimocarli.starlingtest.business.common.UidFactory
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.model.SavingAmount
import uk.co.massimocarli.starlingtest.model.SavingGoal
import uk.co.massimocarli.starlingtest.model.SgTarget
import uk.co.massimocarli.starlingtest.mvp.BasePresenter
import javax.inject.Inject

class TransactionListPresenterImpl @Inject constructor(val mTransactionUseCase: TransactionUseCase, val uidFactory: UidFactory) : BasePresenter<TransactionListView>(), TransactionListPresenter {

    companion object {
        val SAVING_ACCOUNT_AMOUNT = 100000
    }

    /**
     * This method creates (or update) the SavingGoal with the given name and add to it the given amount
     */
    override fun saveMoneyToSavingGoal(name: String, model: List<RoundedUpTransaction>) {
        val totalRoundUp = mTransactionUseCase.calculateTotalRoundUp(model)
        view?.showLoadingDialog()
        val newSavingGoal = SavingGoal(name, uidFactory.getSavingsGoalUid(), target = SgTarget(minorUnits = SAVING_ACCOUNT_AMOUNT))
        val disposable = mTransactionUseCase.createSavingGoal(newSavingGoal)
                .subscribeOn(Schedulers.io())
                .flatMap { response ->
                    val minorUnitsAmount = (totalRoundUp * 100).toInt()
                    val savingAmount = SavingAmount(uidFactory.getTransferUid(), SgTarget(minorUnits = minorUnitsAmount))
                    mTransactionUseCase.addMoneyToSavingGoal(newSavingGoal, savingAmount)
                }
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { response, error ->
                    if (error != null) {
                        view?.showError(error)
                        view?.dismissLoadingDialog()
                    } else {
                        loadTransaction()
                    }
                }
        registerDisposable(disposable)
    }

    /**
     * This method loads the information about transaction
     */
    override fun loadTransaction() {
        view?.showLoadingDialog()
        val disposable = mTransactionUseCase.getTransactionList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { response, error ->
                    if (error != null) {
                        view?.showError(error)
                    } else {
                        view?.showTransactionList(response);
                    }
                    view?.dismissLoadingDialog()
                }
        registerDisposable(disposable)
    }

    /**
     * Invoke the UseCase in order to get the total and then it displays it
     */
    override fun displayTotalRoundUp(model: List<RoundedUpTransaction>) {
        val totalRoundUp = mTransactionUseCase.calculateTotalRoundUp(model)
        view?.showTotalRoundUp(totalRoundUp)
    }
}