package uk.co.massimocarli.starlingtest.business.repository

import io.reactivex.Single
import uk.co.massimocarli.starlingtest.business.networking.StarlingApi
import uk.co.massimocarli.starlingtest.model.SavingAmount
import uk.co.massimocarli.starlingtest.model.SavingGoal
import uk.co.massimocarli.starlingtest.model.SavingGoalResponse
import uk.co.massimocarli.starlingtest.model.TransactionResponse
import javax.inject.Inject

class TransactionRepositoryImpl @Inject constructor(val mStarlingApi: StarlingApi) : TransactionRepository {

    override fun saveMoneyToSavingGoal(sgId: String, savingAmount: SavingAmount): Single<SavingGoalResponse> =
            mStarlingApi.addMoneyToSavingGoal(sgId, savingAmount.transferUid, savingAmount)

    override fun createSavingGoal(savingGoal: SavingGoal): Single<SavingGoalResponse> =
            mStarlingApi.createSavingGoal(savingGoal.savingsGoalUid, savingGoal)

    override fun getTransactions(): Single<TransactionResponse> = mStarlingApi.getTransaction()
}