package uk.co.massimocarli.starlingtest.business.networking

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import uk.co.massimocarli.starlingtest.model.SavingAmount
import uk.co.massimocarli.starlingtest.model.SavingGoal
import uk.co.massimocarli.starlingtest.model.SavingGoalResponse
import uk.co.massimocarli.starlingtest.model.TransactionResponse

/**
 * This is the interface that defines the service we have to invoke non the server
 * This is not bound to any specific Framework (clean architecture)
 */
interface StarlingApi {

    @GET("/api/v1/transactions")
    fun getTransaction(): Single<TransactionResponse>

    @PUT("/api/v1/savings-goals/{savingsGoalUid}")
    fun createSavingGoal(@Path("savingsGoalUid") savingsGoalUid: String, @Body savingGoal: SavingGoal): Single<SavingGoalResponse>

    @PUT("/api/v1/savings-goals/{savingsGoalUid}/add-money/{transferUid}")
    fun addMoneyToSavingGoal(@Path("savingsGoalUid") savingsGoalUid: String,
                             @Path("transferUid") transferUid: String,
                             @Body savingGoal: SavingAmount): Single<SavingGoalResponse>
}