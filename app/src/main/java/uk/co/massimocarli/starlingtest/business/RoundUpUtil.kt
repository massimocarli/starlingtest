package uk.co.massimocarli.starlingtest.business

import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.model.Transaction

val DIRECTION_OUTBOUND = "OUTBOUND"
val DIRECTION_INBOUND = "INBOUND"

/**
 * Utility function which calculate the roundUp for a given amount.
 * The input is always not negative.
 */
fun roundUp(amount: Double): Double {
    // We have the option to throw an exception in case of positive value
    //check(amount <= 0.0, { "Amount can't be positive! " })
    if (amount >= 0.0) return 0.0
    val diff = (-amount.toInt() + 1).toDouble() + amount
    // If the diff is 1.0 we return the amount itself
    return if (diff == 1.0) 0.0 else diff

}

/**
 * Utility method that decorates the Transaction adding information about the roundup.
 * This calculate roundUp only for DIRECTION_OUTBOUND transaction
 */
fun roundUp(transactions: List<Transaction>): List<RoundedUpTransaction> {
    return transactions
            .map { transaction ->
                val roundUp = if (transaction.direction == DIRECTION_OUTBOUND) roundUp(transaction.amount) else 0.0
                RoundedUpTransaction(transaction, roundUp)
            }
}

/**
 * Calculate the roundUp for OUTBOUND transaction
 */
fun totalRoundUp(transactions: List<RoundedUpTransaction>) =
        transactions
                .filter { it.transaction.direction == DIRECTION_OUTBOUND }
                .fold(0.0, { acc, transaction -> acc + transaction.roundUp })