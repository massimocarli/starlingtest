package uk.co.massimocarli.starlingtest.dagger

import android.content.Context
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.massimocarli.starlingtest.ACCESS_TOKEN
import uk.co.massimocarli.starlingtest.business.networking.StarlingApi
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        val THREAD_POOL_SIZE = 5
        val CACHE_SIZE = 10 * 1024 * 1024 // 10 MB
        val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
        val BASE_URL = "https://api-sandbox.starlingbank.com"
        val AUTH_HEADER = "Authorization"
        val AUTH_VALUE_FORMAT = "Bearer %s"
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(context: Context): OkHttpClient {
        val cache = Cache(context.cacheDir, CACHE_SIZE.toLong())
        return OkHttpClient.Builder()
                .addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val originalRequest = chain.request()
                        val reqBuilder = originalRequest.newBuilder()
                        reqBuilder.addHeader(AUTH_HEADER, String.format(AUTH_VALUE_FORMAT, ACCESS_TOKEN))
                        val newRequest = reqBuilder.build()
                        return chain.proceed(newRequest);
                    }
                })
                .cache(cache)
                .build()
    }

    @Provides
    @Singleton
    fun provideSkyScannerApi(okHttpClient: OkHttpClient): StarlingApi {
        val gson = GsonBuilder().setDateFormat(DATE_FORMAT).create()
        val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .callbackExecutor(Executors.newFixedThreadPool(THREAD_POOL_SIZE))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create<StarlingApi>(StarlingApi::class.java)
    }

}