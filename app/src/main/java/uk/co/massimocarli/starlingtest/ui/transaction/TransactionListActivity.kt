package uk.co.massimocarli.starlingtest.ui.transaction

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_transaction_list.*
import kotlinx.android.synthetic.main.content_main.*
import uk.co.massimocarli.starlingtest.R
import uk.co.massimocarli.starlingtest.StarlingApp
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.ui.common.BaseActivity
import uk.co.massimocarli.starlingtest.ui.common.LoadingDialogViewImpl

class TransactionListActivity : BaseActivity<TransactionListPresenter, TransactionListView>(), TransactionListView {

    lateinit var mTransactionAdapter: TransactionAdapter
    var model = mutableListOf<RoundedUpTransaction>()
    var loadingDialogView = LoadingDialogViewImpl(this)

    override fun showError(error: Throwable) {
        Snackbar.make(transactionRecyclerView, error.localizedMessage, Snackbar.LENGTH_LONG).show()
    }

    override fun showTransactionList(transactionList: List<RoundedUpTransaction>) {
        val diffCallback = TransactionDiff(model, transactionList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        model.clear()
        model.addAll(transactionList)
        diffResult.dispatchUpdatesTo(mTransactionAdapter)
        transactionRecyclerView.scrollToPosition(0)
        mPresenter?.displayTotalRoundUp(transactionList)
    }

    override fun showTotalRoundUp(totalRoundUp: Double) {
        roundUpTotal.text = getString(R.string.total_round_up, totalRoundUp)
    }

    override fun showLoadingDialog() {
        // I wanted to use delegation but I cannot pass the context in the declaration
        // of the Activity
        loadingDialogView.showLoadingDialog()
    }

    override fun dismissLoadingDialog() {
        loadingDialogView.dismissLoadingDialog()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_list)
        (application as StarlingApp).getStarlingComponent().inject(this)
        setSupportActionBar(toolbar)
        transactionRecyclerView.setHasFixedSize(true)
        transactionRecyclerView.setLayoutManager(LinearLayoutManager(this))
        mTransactionAdapter = TransactionAdapter(model)
        transactionRecyclerView.adapter = mTransactionAdapter
        sendRoundUpButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // Generate a pseudo-random name for the SavingGoal. It could have been generated
                // in different way or requested to the user
                val savingName = "Saving #${System.currentTimeMillis() % 100000}"
                mPresenter?.saveMoneyToSavingGoal(savingName, model)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        mPresenter?.loadTransaction()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_reload -> {
                mPresenter?.loadTransaction()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
