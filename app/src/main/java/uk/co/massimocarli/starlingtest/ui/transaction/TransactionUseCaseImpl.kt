package uk.co.massimocarli.starlingtest.ui.transaction

import io.reactivex.Single
import uk.co.massimocarli.starlingtest.business.repository.TransactionRepository
import uk.co.massimocarli.starlingtest.business.roundUp
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.model.SavingAmount
import uk.co.massimocarli.starlingtest.model.SavingGoal
import uk.co.massimocarli.starlingtest.model.SavingGoalResponse
import java.util.*
import javax.inject.Inject

/**
 * It implements the business logic for the TransactionList
 */
class TransactionUseCaseImpl @Inject constructor(val mTransactionRepository: TransactionRepository) : TransactionUseCase {

    override fun addMoneyToSavingGoal(savingGoal: SavingGoal, savingAmount: SavingAmount): Single<SavingGoalResponse> {
        savingAmount.transferUid = UUID.randomUUID().toString();
        return mTransactionRepository.saveMoneyToSavingGoal(savingGoal.savingsGoalUid, savingAmount)
    }

    override fun createSavingGoal(savingGoal: SavingGoal): Single<SavingGoalResponse> {
        savingGoal.savingsGoalUid = UUID.randomUUID().toString();
        return mTransactionRepository.createSavingGoal(savingGoal)
    }

    override fun getTransactionList(): Single<List<RoundedUpTransaction>> {
        return mTransactionRepository.getTransactions().flatMap { response ->
            Single.just(roundUp(response._embedded.transactions))
        }
    }

    override fun calculateTotalRoundUp(transactions: List<RoundedUpTransaction>): Double =
            transactions.sumByDouble { it.roundUp }

}