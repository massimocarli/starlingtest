package uk.co.massimocarli.starlingtest.mvp

import androidx.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Tagging interface for View objects
 */
interface View

/**
 * Presenter abstraction for a specific View
 */
interface Presenter<View> {

    /**
     * Invoked during the specific View lifecycle in order to get pass the presenter the
     * related View
     *
     * @param view The View used from the presenter
     */
    fun attach(view: View)

    /**
     * Invoked during the View lifecycle in order to release resources
     */
    fun detach()
}

/**
 * The base implementation for the Presenter. It manages View lifecycle
 */

abstract class BasePresenter<V : View> : Presenter<V> {

    protected var view: V? = null
        private set

    /**
     * For Resource management
     */
    private var compositeDisposable: CompositeDisposable? = null

    var isAttached: Boolean = false
        private set

    @CallSuper
    override fun attach(v: V) {
        compositeDisposable = CompositeDisposable()
        view = v
        isAttached = true
    }

    @CallSuper
    override fun detach() {
        isAttached = false
        view = null
        compositeDisposable!!.dispose()
    }

    protected fun registerDisposable(disposable: Disposable) {
        compositeDisposable!!.add(disposable)
    }
}

/**
 * Tagging interface for UseCase
 */
interface UseCase

/**
 * Tagging interface for Repository
 */
interface Repository