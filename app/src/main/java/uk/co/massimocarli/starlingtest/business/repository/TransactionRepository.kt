package uk.co.massimocarli.starlingtest.business.repository

import io.reactivex.Single
import uk.co.massimocarli.starlingtest.model.SavingAmount
import uk.co.massimocarli.starlingtest.model.SavingGoal
import uk.co.massimocarli.starlingtest.model.SavingGoalResponse
import uk.co.massimocarli.starlingtest.model.TransactionResponse
import uk.co.massimocarli.starlingtest.mvp.Repository

/**
 * This is the abstraction for the Repository for Transaction
 */
interface TransactionRepository : Repository {

    /**
     * Provide the information about a transaction request
     */
    fun getTransactions(): Single<TransactionResponse>

    /**
     * Allow the creation of a saving goal
     */
    fun createSavingGoal(savingGoal: SavingGoal): Single<SavingGoalResponse>

    /**
     * Allow the creation of a saving goal
     */
    fun saveMoneyToSavingGoal(sgId: String, savingAmount: SavingAmount): Single<SavingGoalResponse>
}