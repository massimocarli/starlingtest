package uk.co.massimocarli.starlingtest

import android.app.Application
import uk.co.massimocarli.starlingtest.dagger.*

class StarlingApp : Application() {

    val component: StarlingComponent by lazy {
        DaggerStarlingComponent
                .builder()
                .networkModule(NetworkModule())
                .appModule(AppModule(this))
                .build()
    }

    fun getStarlingComponent(): StarlingComponent = component
}