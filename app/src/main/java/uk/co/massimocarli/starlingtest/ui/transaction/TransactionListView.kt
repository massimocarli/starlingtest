package uk.co.massimocarli.starlingtest.ui.transaction

import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction
import uk.co.massimocarli.starlingtest.mvp.View
import uk.co.massimocarli.starlingtest.ui.common.LoadingDialogView

/**
 * The View interface for the TransactionList
 */
interface TransactionListView : View, LoadingDialogView {

    /**
     * Displays the transactions into the View
     */
    fun showTransactionList(transactionList: List<RoundedUpTransaction>)

    /**
     * Displays error message
     */
    fun showError(error: Throwable)

    /**
     * Displays the total roundup
     */
    fun showTotalRoundUp(totalRoundUp: Double)
}