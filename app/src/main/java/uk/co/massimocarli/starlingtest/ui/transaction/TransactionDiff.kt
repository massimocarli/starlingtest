package uk.co.massimocarli.starlingtest.ui.transaction

import androidx.recyclerview.widget.DiffUtil
import uk.co.massimocarli.starlingtest.model.RoundedUpTransaction

/**
 * Utility class in order to detect differences
 */
class TransactionDiff(val before: List<RoundedUpTransaction>, val after: List<RoundedUpTransaction>) : DiffUtil.Callback() {

    // They are the same if they have the same Id
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            before[oldItemPosition].transaction.id == after[newItemPosition].transaction.id

    override fun getOldListSize(): Int = before.size

    override fun getNewListSize(): Int = after.size

    // They are the same if they have the same Id
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            before[oldItemPosition].transaction.id == after[newItemPosition].transaction.id
}