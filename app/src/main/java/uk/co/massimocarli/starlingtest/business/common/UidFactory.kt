package uk.co.massimocarli.starlingtest.business.common

/**
 * Abstraction that allows us to manage Guid
 */
interface UidFactory {

    /**
     * The Guid for the Saving Goal
     */
    fun getSavingsGoalUid(): String

    /**
     * The Guid for the Transfer
     */
    fun getTransferUid(): String
}