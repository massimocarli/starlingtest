package uk.co.massimocarli.starlingtest.business.common

import java.util.*

/**
 * UidFactory implementation that creates the uid. We use constant values for this test
 */
class UidFactoryImpl : UidFactory {

    companion object {
        // We use constant values now
        val SAVING_GOAL_UID = UUID.randomUUID().toString();
        val TRANSFER_UID = UUID.randomUUID().toString();
    }

    override fun getSavingsGoalUid(): String = SAVING_GOAL_UID

    override fun getTransferUid(): String = TRANSFER_UID
}