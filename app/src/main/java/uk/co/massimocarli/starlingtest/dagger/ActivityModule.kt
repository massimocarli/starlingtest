package uk.co.massimocarli.starlingtest.dagger

import dagger.Module
import dagger.Provides
import uk.co.massimocarli.starlingtest.business.common.UidFactory
import uk.co.massimocarli.starlingtest.ui.transaction.TransactionListPresenter
import uk.co.massimocarli.starlingtest.ui.transaction.TransactionListPresenterImpl
import uk.co.massimocarli.starlingtest.ui.transaction.TransactionUseCase

@Module
class ActivityModule {

    @Provides
    fun provideTransactionListPresenter(transactionListUseCase: TransactionUseCase, uidFactory: UidFactory): TransactionListPresenter =
            TransactionListPresenterImpl(transactionListUseCase, uidFactory)
}